public class PrintGrade{
	public static void main(String[] args){
		//System.out.println(args);
		int note = Integer.parseInt(args[0]);
		if (note >= 90)
			System.out.println("A");
		else if (note >= 80)
            System.out.println("B");
		else if (note >= 70)
            System.out.println("C");
		else if (note > 60)
            System.out.println("D");
		else if (note<60)
			System.out.println("F");
	}
}
