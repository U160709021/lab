public class Int2Bin{
	public static void main(String[] args){
		String method = args[0];
		int number = Integer.parseInt(args[1]);
		if (method .equals("loop"))
			System.out.println(loop(number));
		else if (method.equals("rec"))
			System.out.println(rec(number));
		else
			System.out.println("Please enter the valid function !!");
	}
	
	private static String rec(int num){
		if (num == 0 || num == 1)
			return num + "";
		return rec(num/2) + num % 2;
	}
	public static String loop(int num){
		String s = "";
		while (num > 0){
			s =  ( (num % 2 ) == 0 ? "0" : "1") + s;
			num = num / 2;
		}
		return s;
	}
}
