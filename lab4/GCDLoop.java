public class GCDLoop{
	
	public static void main(String[] args){
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		System.out.println(gcd(a,b));	
	}
	private static int gcd(int a, int b){
		int min=a>b?b:a,max=a+b-min,c=0;
		while (min!=0){
			c = max%min;
			max = min;
			min = c;
		}	
		return max;
	}
}
