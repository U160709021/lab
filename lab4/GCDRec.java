public class GCDRec{
	public static void main(String[] args){
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
	    System.out.println(gcd(a,b));
	//	return ( b == 0 ? a : main(b, a % b) );

	}
	private static int gcd(int a,int b) {
		return ( b == 0 ? a : gcd(b, a % b) );
	}
}
