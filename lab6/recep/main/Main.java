package recep.main;

import recep.shapes.Circle;
import recep.shapes.Square;
import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args){
        Circle circle1 = new Circle(2);
        Circle circle2 = new Circle(3);
        Circle circle3 = new Circle(4);
        ArrayList<Circle> circles = new ArrayList<Circle>();
        circles.add(circle1);
        circles.add(circle2);
        circles.add(circle3);
        System.out.println("Circles areas : ");
        for (Circle c : circles){
            System.out.println("\t> "+c.area());
        }

        Square square = new Square(2);
        System.out.println("\nSquare area is "+square.area());
    }
}
