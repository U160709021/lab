package recep.shapes;

public class Square {
    protected int side;

    public Square(int i){
        this.side = i;
    }

    public double area(){
        return side*side;
    }
}
