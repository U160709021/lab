package recep.shapes;

public class Circle {
    protected int radius;

    public Circle(int r){
        radius = r;
    }
    public double area(){
        return (2*3.14)*(radius*radius);
    }
}
