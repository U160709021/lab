public class Rectangle{
    int sideA;
    int sideB;

    public Rectangle(int a, int b){
        sideA = a;
        sideB = b;
    }

    public int area(){
        return sideB * sideA;
    }

    public int perimeter(){
        return 2*(sideA+sideB);
    }
}