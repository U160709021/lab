public class Circle {
    int radius;

    public Circle(int a){
        radius = a;
    }
    public double area(){
        return 3.14 * radius * radius;
    }

    public double perimeter(){
        return 2 * 3.14 * radius;
    }
}
