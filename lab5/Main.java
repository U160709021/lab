public class Main{

    public static void main(String[] args){
        System.out.println("\nRectangle");
        Rectangle rectA = new Rectangle(5,6);
        System.out.println("\nSide A : " + rectA.sideA+" | Side B : " + rectA.sideB );
        int area = rectA.area();
        System.out.println("Rectangle area : " + area);
        System.out.println("Rectangle perimeter : " + rectA.perimeter());


        System.out.println("\n\nCircle");
        Circle circle = new Circle(10);
        System.out.println("\nRadius : "+ circle.radius );
        System.out.println("Circle area : " + circle.area());
        System.out.println("Circle perimeter : " + circle.perimeter());

    }

}