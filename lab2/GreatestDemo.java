public class GreatestDemo{
    public static void main(String[] args){
        
        int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]); 

        Integer bigger = null;

        if (value1 > value2)
            bigger = value1;
        else
            bigger = value2;

        if (bigger > value3)
            System.out.println(bigger);
        else
            System.out.println(value3);
    }
}
