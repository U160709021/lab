package recep.main;
import recep.shapes3d.*;

import java.util.ArrayList;

public class Test3D {

    public static void main(String[] args){
        ArrayList<Cylinder> cylinders = new ArrayList<>();
        Cylinder cylinder = new Cylinder();

        cylinders.add(cylinder);
        cylinders.add(new Cylinder(6,7));
        System.out.println(cylinders.toString());

        ArrayList<Box> boxes = new ArrayList<>();
        Box box = new Box(6);
        boxes.add(box);
        boxes.add(new Box(5));
        System.out.println(boxes.toString());
    }

}
