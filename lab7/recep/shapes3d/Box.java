package recep.shapes3d;

import recep.shapes.Square;

public class Box extends Square{

    public Box(){
        this(5);
    }

    public Box(int s){
        super(s);
    }

    public double area(){
        return 6 * super.area();
    }

    public double volume(){
        return super.area() * side;
    }

    public String toString(){
        return "Box area = "+ area() +
                ", volume = " + volume();
    }

}
