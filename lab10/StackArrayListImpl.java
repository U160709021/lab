package stack;
import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl <T> implements Stack <T>{
 
	 
	
	 private ArrayList<T>  stacklist = new ArrayList<T>(); 
	
	//protected T t;
    	
	
	@Override
	public void push(T t) {
		stacklist.add(t);
	}

	@Override
	public T pop() {
        
	return stacklist.remove(stacklist.size() -1);
	//return t;
	}

	@Override
	public boolean empty() {
		
		return stacklist.size()==0;
	}

	@Override
	public List<T> toList() {
		return stacklist;
	}

}
