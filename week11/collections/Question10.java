package collections;
//
//import java.util.HashSet;
//import java.util.Set;
//
//public class Question2 {
//	int value;
//	public static void main(String[] args) {
//		Set<Question2> set = new HashSet<>();
//		set.add(new Question2(5));
//		set.add(new Question2(4));
//		set.add(new Question2(3));
//		set.add(new Question2(4));
//		set.add(new Question2(2));
//		System.out.println(set.size());
//		//System.out.println(set);
//		Question2 a = new Question2(2);
//		Question2 b = new Question2(2);
//		System.out.println(a.equals(b));
//
//	}
//	public Question2(int value){
//		this.value = value;
//	}
//
//	public boolean equals(Object o){
//		if (o instanceof Question2){
//			return value == ((Question2)o).value;
//		}
//		return false;
//	}
//}

//import java.util.ArrayList;
//		import java.util.List;
//public class Question3 {
//	public static void main(String[] args) {
//		List<Integer> list = new ArrayList<>();
//		list.add(5);
//		list.add(7);
//		list.add(10);
//		list.add(4);
//		System.out.println(list);
//
//		for (int i = 0; i < list.size(); i++) {
//			list.remove(i);
//			System.out.println(list);
//		}
//		System.out.println(list);
//	}
//}
//
//import java.util.LinkedHashSet;
//import java.util.Set;
//public class Question4 implements Comparable<Question4>{
//	String value;
//	public static void main(String[] args) {
//		Set<Question4> set = new LinkedHashSet<>();
//		set.add(new Question4("c"));
//		set.add(new Question4("a"));
//		set.add(new Question4("ab"));
//		set.add(new Question4("bc"));
//		set.add(new Question4("ab"));
//		System.out.println(set);
//	}
//	public Question4(String value){
//		this.value = value;
//	}
//
//	public int compareTo(Question4 q) {
//		return q.value.length() - value.length();
//	}
//	public String toString(){
//		return value;
//	}
//	@Override
//	public boolean equals(Object o) {
//		if (o instanceof Question4){
//			return value == ((Question4)o).value;
//		}
//		return false;
//	}
//
//}

//import java.util.Comparator;
//import java.util.Map;
//import java.util.TreeMap;
//public class Question5 implements Comparator<String> {
//	public int compare(String q1, String q2) {
//		return q1.length() - q2.length();
//	}
//	public static void main(String[] args) {
//		Map<String, Integer> map = new TreeMap<>(new Question5());
//		map.put("abc", 1);
//		map.put("a", 3);
//		map.put("bc", 2);
//		map.put("ab", 1);
//		map.put("ab", 5);
//		map.put("abc", 7);
//		System.out.println(map);
//	}
//}
//
//public class Question6 extends Exception{
//	public static void main(String[] args) {
//		try {
//			badMethod();
//			System.out.print("A");
//		} catch (RuntimeException ex) {
//			System.out.print("B");
//		} catch (Exception ex1) {
//			System.out.print("C");
//		} finally {
//			System.out.print("D");
//		}
//		System.out.print("E");
//	}
//	public static void badMethod() throws Question6{
//		throw new Question6();
//	}
//}

//public class Question7 {
//	public static void main(String[] args) {
//		try {
//			String a = null;
//			System.out.println(a.length());
//		} catch (Exception e) {
//			System.out.println("Exception");
//		} catch (RuntimeException re) {
//			System.out.println(" Runtime Exception");
//		}
//		System.out.println("finished");
//	}
//}

//public class Question8 {
//	public static void main(String [] args) {
//		try {
//			badMethod();
//			System.out.print("A");
//		}catch (Error ex){
//			System.out.print("B");
//		}finally{
//			System.out.print("C");
//		}
//		System.out.print("D");
//	}
//	public static void badMethod(){
//		throw new RuntimeException();
//	}
//}

//public class Question9 {
//	public static void main(String[] args) {
//		try {
//			method1();
//		} finally {
//			System.out.print("Z");
//		}
//	}
//	private static void method1() {
//		System.out.print("A");
//		try {
//			method2();
//		} catch (NullPointerException ex) {
//			System.out.print("U");
//			if (1 == 1)
//				return;
//			System.out.print("V");
//		} catch (Exception ex) {
//			System.out.println("E");
//		} finally {
//			System.out.print("X");
//		}
//		System.out.print("Y");
//	}
//	private static void method2() {
//		try {
//			System.out.print("B");
//			String a = null;
//			a.length();
//		} catch (Exception ex) {
//			throw ex;
//		} finally {
//			System.out.print("C");
//		}
//	}
//}


public class Question10 {
	public static void main(String[] args) {
		try {
			method(0);
			System.out.print("C");
		} catch(Exception ex) {
			System.out.print("D");
		} finally {
			System.out.print("E");
		}
	}
	private static void method(int a) {
		if (a == 0) {
			throw new IllegalArgumentException();
		}
		try {
			System.out.println("A");
		} finally {
			System.out.println("B");
		}
	}
}